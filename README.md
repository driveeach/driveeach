# Drive EACH

## Repositórios
- [Ciclo Básico](https://gitlab.com/driveeach/drivecb)
- [Educação Física e Saúde](https://gitlab.com/driveeach/driveefs)
- [Gerontologia](https://gitlab.com/driveeach/drivegeronto)
- [Gestão Ambiental](https://gitlab.com/driveeach/drivega)
- [Gestão de Políticas Públicas](https://gitlab.com/driveeach/drivegpp)
- [Lazer e Turismo](https://gitlab.com/driveeach/drivelzt)
- [Licenciatura em Ciências da Natureza](https://gitlab.com/driveeach/drivelcn)
- [Marketing](https://gitlab.com/driveeach/drivemkt)
- [Obstetrícia](https://gitlab.com/driveeach/driveobs)
- [Sistemas de Informação](https://gitlab.com/driveeach/drivesi)
- [Têxtil e Moda](https://gitlab.com/driveeach/drivetm)



## O que é
O **Drive EACH** visa ajudar estudantes da Escola de Artes, Ciências e Humanidades da Universidade de São Paulo a contribuir material para estudo a outros estudantes. Um espaço público para todos os estudantes se ajudarem.

## Por que?
A dificuldade que a **Universidade de São Paulo oferece** aos seus estudantes torna um espaço como esse necessário. O compartilhamento de material de estudo entre estudantes é feito comumente com cópias e fotos de cadernos e exercícios resolvidos. No entanto, esse compartilhamento manual possui um escopo pequeno e pode não ajudar todos os estudantes. O **Drive EACH** pretende acabar com essa barreira e ajudar todo mundo!!

## Como contribuir?
Se você está acostumado com repositórios GIT, simplesmente crie um pull-request para qualquer repositório que você adicionar sua contribuição.

Se você não está acostumado com GIT e deseja contribuir, por favor siga [este tutorial](https://gitlab.com/driveeach/driveeach/wikis/Contribuindo) e envie-nos um *issue* no repositório que você quiser contribuir.

Ou simplesmente envie um email **detalhando sua contribuição** para **driveeach@gmail.com**
